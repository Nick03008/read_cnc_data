﻿namespace Read_CNC_data
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtMachZ_F = new System.Windows.Forms.TextBox();
            this.txtMachY_F = new System.Windows.Forms.TextBox();
            this.txtMachX_F = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtAbsZ_F = new System.Windows.Forms.TextBox();
            this.txtAbsY_F = new System.Windows.Forms.TextBox();
            this.txtAbsX_F = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "x :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "y :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "z :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 18);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 5;
            this.label6.Text = "x :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "y :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "z :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtMachZ_F);
            this.groupBox1.Controls.Add(this.txtMachY_F);
            this.groupBox1.Controls.Add(this.txtMachX_F);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(22, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "機械作標";
            // 
            // txtMachZ_F
            // 
            this.txtMachZ_F.Location = new System.Drawing.Point(73, 69);
            this.txtMachZ_F.Name = "txtMachZ_F";
            this.txtMachZ_F.Size = new System.Drawing.Size(100, 22);
            this.txtMachZ_F.TabIndex = 5;
            // 
            // txtMachY_F
            // 
            this.txtMachY_F.Location = new System.Drawing.Point(73, 41);
            this.txtMachY_F.Name = "txtMachY_F";
            this.txtMachY_F.Size = new System.Drawing.Size(100, 22);
            this.txtMachY_F.TabIndex = 5;
            // 
            // txtMachX_F
            // 
            this.txtMachX_F.Location = new System.Drawing.Point(73, 15);
            this.txtMachX_F.Name = "txtMachX_F";
            this.txtMachX_F.Size = new System.Drawing.Size(100, 22);
            this.txtMachX_F.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtAbsZ_F);
            this.groupBox2.Controls.Add(this.txtAbsY_F);
            this.groupBox2.Controls.Add(this.txtAbsX_F);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(249, 59);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(204, 100);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "絕對作標";
            // 
            // txtAbsZ_F
            // 
            this.txtAbsZ_F.Location = new System.Drawing.Point(56, 66);
            this.txtAbsZ_F.Name = "txtAbsZ_F";
            this.txtAbsZ_F.Size = new System.Drawing.Size(100, 22);
            this.txtAbsZ_F.TabIndex = 5;
            // 
            // txtAbsY_F
            // 
            this.txtAbsY_F.Location = new System.Drawing.Point(56, 38);
            this.txtAbsY_F.Name = "txtAbsY_F";
            this.txtAbsY_F.Size = new System.Drawing.Size(100, 22);
            this.txtAbsY_F.TabIndex = 5;
            // 
            // txtAbsX_F
            // 
            this.txtAbsX_F.Location = new System.Drawing.Point(56, 15);
            this.txtAbsX_F.Name = "txtAbsX_F";
            this.txtAbsX_F.Size = new System.Drawing.Size(100, 22);
            this.txtAbsX_F.TabIndex = 5;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(459, 622);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtMachZ_F;
        private System.Windows.Forms.TextBox txtMachY_F;
        private System.Windows.Forms.TextBox txtMachX_F;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtAbsZ_F;
        private System.Windows.Forms.TextBox txtAbsY_F;
        private System.Windows.Forms.TextBox txtAbsX_F;
        private System.Windows.Forms.Timer timer1;
    }
}

