﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using InterfaceLib;
using MySql.Data.MySqlClient;

namespace Read_CNC_data
{

    public class mechine
    {
        // primarily key
        public int mechineID;
        public InterfaceLib.IMsg iRemoting = null;
        public StructMsg.position _position;
        public StructMsg.spindle_temperature _spindle_temperature;
        public StructMsg.spindle_load _spindle_load;
        public StructMsg.status _status;
        public StructMsg.gcode _gcode;
        public StructMsg.Pwd _Pwd;
        public int mechinePort = 0;


        // 物件初始化
        public mechine(int mechineID,int mechinePort , string pwd)
        {
            this.mechinePort = mechinePort;
            this._Pwd.ConnectionKey = pwd;
            this.mechineID = mechineID;
        }

        // 取得port資料
        public void load()
        {
            this.iRemoting = (IMsg)Activator.GetObject(typeof(IMsg), "tcp://localhost:" + mechinePort.ToString() + "/RemoteObjectURI" + mechinePort.ToString());
        }

        public StructMsg.position updatePosition(MySQLDB DB)
        {
            short ret1  = this.iRemoting.GET_position(this._Pwd, ref this._position);
            short ret2 = this.iRemoting.GET_spindle_temperature(this._Pwd, ref this._spindle_temperature);
            short ret3 = this.iRemoting.GET_spindle_load(this._Pwd, ref this._spindle_load);
            short ret4 = this.iRemoting.GET_status(this._Pwd, ref this._status);
            
            // SkyMars 海德漢不資源找尋GCode
            // short ret5 = this.iRemoting.GET_gcode(this._Pwd, ref this._gcode);    


            //ret 無錯誤的話回傳0 
            if (ret1 + ret2 + ret3 + ret4 == 0)
            {
                DB.updateImmediateDatas(this.mechineID, _position, this._spindle_temperature, this._spindle_load, this._status);
            }
            return _position;
        }
    }

    // 資料庫 儲存 物件

    public class MySQLDB
    {
        string dbHost;
        string dbUser;
        string dbPass;
        string dbName;
        string connstr;
        MySqlConnection conn;

        public MySQLDB(string dbHost, string dbUser, string dbPass, string dbName)
        {
            this.dbHost = dbHost;
            this.dbUser = dbUser;
            this.dbPass = dbPass;
            this.dbName = dbName;
            this.connstr = "server=" + dbHost + ";Uid=" + dbUser + ";Pwd=" + dbPass + ";Database=" + dbName;
            this.conn = new MySqlConnection(this.connstr);
        }


        // 由各機台物件  呼叫  更新資料
        public void updateImmediateDatas(int mechineID, StructMsg.position _position, StructMsg.spindle_temperature _spindle_temperature,
            StructMsg.spindle_load _spindle_load,StructMsg.status _status )
        {
            


            MySqlCommand command = this.conn.CreateCommand();
            conn.Open();

            command.CommandText = string.Format("UPDATE immediate_datas SET "+
                "m_x = {0}," + 
                "m_y = {1}," + 
                "m_z = {2}," + 
                "abs_x = {3}," + 
                "abs_y = {4}," + 
                "abs_z = {5}," +
                "spinderLoad = {6}," +
                "temperature = {7}," +
                "runningCodeIndex = {8}," +
                "created_at = now() , updated_at = now() " +
                "WHERE machine_id = {9}"
            , _position.Mach[0], _position.Mach[1], _position.Mach[2], _position.Abs[0], _position.Abs[1], _position.Abs[2]
            , _spindle_temperature.Spindle_1_Temp
            , _spindle_load.SpLoad
            , _status.CurSeq
            ,mechineID);

            command.ExecuteNonQuery();
            conn.Close();
        }
    }

  
    public partial class Form1 : Form
    {
        public mechine mechine1 = new mechine(1,9501,"pmc");
        public mechine mechine2 = new mechine(2, 9502, "pmc");

        public MySQLDB DB = new MySQLDB("localhost", "root", "root", "project");

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            if (ChannelServices.RegisteredChannels.Length == 0)
                ChannelServices.RegisterChannel(new TcpChannel(), false);

            mechine1.load();
            mechine2.load();

            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            StructMsg.position _position1 = mechine1.updatePosition(this.DB);
            StructMsg.position _position2 = mechine2.updatePosition(this.DB);


            txtMachX_F.Text = _position1.Mach[0].ToString();
            txtMachY_F.Text = _position1.Mach[1].ToString();
            txtMachZ_F.Text = _position1.Mach[2].ToString();
            txtAbsX_F.Text = _position1.Abs[0].ToString();
            txtAbsY_F.Text = _position1.Abs[1].ToString();
            txtAbsZ_F.Text = _position1.Abs[2].ToString();

        }
    }
}
